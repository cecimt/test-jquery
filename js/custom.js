$(document).ready(function(){
    $('.modal').modal()
    $('.sidenav').sidenav();

    $('#modal3').modal({
        onOpenStart : function (event) {
            const title = $('.card-title').text();
            $('#modal3 .modal-content strong').text(title);
        }
    });
});